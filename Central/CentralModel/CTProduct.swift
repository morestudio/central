//
//  CTProduct.swift
//  Central
//
//  Created by nickay on 7/9/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit


class CTProduct: MSProduct {
   
    
    
    let keySizes = "Sizes"
    let keyColors = "Colors"
    let keyCapacity = "Capacity"

    var sizes: [String] = []
    var colors: [String] = []
    var capacity: [String] = []
    
    
    override func config(info: JSON) {
        
        
    }
}

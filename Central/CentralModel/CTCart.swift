//
//  CTCart.swift
//  Central
//
//  Created by nickay on 7/13/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class CTCart: MSCart {
   
    
     func checkOutCart(coupons: [MSCoupon]?, giftItems: AnyObject?, completionHandler: ((success: Bool, error: NSError?) -> Void)?)
     {

        self.checkOutCart(coupons, customObjects: giftItems, completionHandler: completionHandler)
    }
    
    
}

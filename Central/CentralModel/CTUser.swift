//
//  CTUser.swift
//  Central
//
//  Created by nickay on 7/13/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class CTUser: MSUser
{
//    let keyUserName = "userName"
//    let keyPassword = "password"
//    let keyEmail = "email"
//    let keyFirstName = "firstName"
//    let keyLastName = "lastName"
//    let keyNickName = "nickName"
//    let keyImage = "image"
//    let keyFacebookID = "facebookID"
    let keyThe1Card = "The1CardNumber"
    let keyDiscountPercent = "DiscountPercent"
    let keyPaymentCode = "PaymentCode"
    let keyCustId = "CustId"
    
    var the1CardNumber :String!
    var discountPercent: Int!
    var paymentCode :String!
    var custId:String!
    
    
//    override class var currentUser: CTUser {
//        struct Static {
//            static var onceToken: dispatch_once_t = 0
//            static var instance: CTUser? = nil
//        }
//        dispatch_once(&Static.onceToken, { () -> Void in
//            Static.instance = CTUser()
//        })
//        return Static.instance!
//    }
    override class var currentUser: CTUser?
    {
       
        if let returnUser = _currentUser as? CTUser
        {
            return returnUser
        }
        return nil
    }
    override func config(info: JSON)
    {
        super.config(info)
        self.the1CardNumber = info[keyThe1Card].stringValue
        self.discountPercent = info[keyDiscountPercent].int
        self.paymentCode = info[keyPaymentCode].stringValue
        self.custId = info[keyCustId].stringValue

    }
    
    class func loginWithCentralOnlineAccount(accountId:String , password:String , completionHandler: ((user:CTUser?, error:NSError?) -> Void)?)
    {
//       
//       MSUser.logInWithUserName( accountId, password: password) { (user, error) -> Void in
//        
//        
//      if error != nil
//      {
//            var newUser = CTUser(info:user!.info)
//            completionHandler?(user: newUser,error: nil)
//      }
//     else
//      {
//            completionHandler?(user: nil,error: error)
//        }
//        
//    }
     
    }
    class func loginwithTheOneCartAccount(theOneCareID:String , password:String , completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        MSUser.logInWithUserName(theOneCareID, password: password, completionHandler: completionHandler)
        
    }
    
    
    func signUpNewUserWithThe1Card(completionHandler: ((user:CTUser?, error:NSError?) -> Void)?)
    {

      CTAPIService.manager.userSignUpWithThe1Card(self, completionHandler: completionHandler)
        
//        CTAPIService.manager.userSignUpWithThe1Card(user: self) { (user, error) -> Void in
//            
//        }
        
    }
    
    override class func logInWithUserName (username:String,password:String,completionHandler: ((user:CTUser?, error:NSError?) -> Void)?)
    {
        
        
        MSUser.logInWithUserName( username, password: password) { (user, error) -> Void in
            
            
            if error != nil
            {
                
                
                completionHandler?(user: nil,error: error)
            }
            else
            {
                var newUser = CTUser(info:user!.info)
                self._currentUser  =  newUser
                CTUser.currentUser!.config(user!.info)
                
                completionHandler?(user: newUser,error: nil)
            }
        }
        
    }
    
    
   
}

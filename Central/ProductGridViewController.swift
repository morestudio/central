//
//  ProductGridViewController.swift
//  Central
//
//  Created by nickay on 7/16/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class ProductGridViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ProductHeaderViewDelegate{

    var products:[MSProduct] = []
    let numberOfGridePerLine:CGFloat  = 2
    let minSpacing:CGFloat  = 10
    var cellIdentifier: String = ""
    let ListIdentifier:String = "ListProductView"
    let GridIdentifier:String = "GridProductView"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: ProductHeaderViewDelegate
    func didPressSwapLayoutToggle(atIndex: Int) {
        
        if cellIdentifier == ListIdentifier
        {
            cellIdentifier = GridIdentifier
        }
        else
        {
            cellIdentifier = ListIdentifier
        }
  
       
        
        self.collectionView.performBatchUpdates(nil, completion: { (finished:Bool) -> Void in
            
            if finished
            {
                 self.collectionView.reloadItemsAtIndexPaths(self.collectionView.indexPathsForVisibleItems())
            }
        })
    }
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 2
    }
    //   optional 
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        if section == 0
        {
            return CGSizeZero
        }
        else
        {
            return CGSizeMake(self.view.frame.size.width, 50)
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            return self.products.count
        }
        
    }
    
     func collectionView(collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "HeaderSection",
                    forIndexPath: indexPath)
                as! ProductHeaderView
                headerView.headerAtIndex = indexPath.section
                headerView.delegate = self
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }
    
   
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if(indexPath.section == 0 )
        {
           // Headercell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Headercell", forIndexPath: indexPath) as! UICollectionViewCell
            return cell

        }
        /*
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DisplayCell
        
        if cell.displayView == nil
        {
        cell.displayView = UIView.loadFromNibNamed("GridProductView")
        
        if let girdView = cell.displayView as? GridProductView
        {
        // TO DO CONFIG
        girdView.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)
        
        
        cell.contentView.addSubview(girdView)
        }
        
        }
        
        if let girdView = cell.displayView as? GridProductView
        {
        // TO DO CONFIG
        
        let product:MSProduct = self.products[indexPath.row-1]
        girdView.config(product)
        }*/

        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! DisplayCell
       
        
        
        if(cell.displayView == nil)
        {
            cell.displayView = UIView.loadFromNibNamed(self.cellIdentifier)
            cell.contentView.addSubview(cell.displayView)
            
            Helper.deley(time: 0) { () -> Void in
                cell.displayView.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)
        
            }
            
        }


        if let listProductView = cell.displayView as? ListProductView
        {
            // TO DO CONFIG
            
            let product:MSProduct = self.products[indexPath.row]
            listProductView.config(product)
        }
        if let girdView = cell.displayView as? GridProductView
        {
            // TO DO CONFIG
            
            let product:MSProduct = self.products[indexPath.row]
            girdView.config(product)
        }

        return cell

    }
    /*
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
    if (indexPath.row == 0)
    {
    let width:CGFloat = (self.view!.frame.size.width)
    return CGSizeMake(width, 200)
    
    }
    
    let width:CGFloat = (self.view!.frame.size.width)
    return CGSizeMake(width, 132)
    
    /*
    let width:CGFloat = (self.view!.frame.size.width-(minSpacing * (numberOfGridePerLine - 1 )) ) / numberOfGridePerLine
    
    return CGSizeMake(width, width*3/2)*/
    }
*/
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if (indexPath.section == 0)
        {
            let width:CGFloat = (self.view!.frame.size.width)
            return CGSizeMake(width, 200)

        }
        else if self.cellIdentifier == GridIdentifier
        {
            let width:CGFloat = (self.view!.frame.size.width-(minSpacing * (numberOfGridePerLine - 1 )) ) / numberOfGridePerLine
            
            return CGSizeMake(width, width*525/300)
        }
        else if self.cellIdentifier == ListIdentifier

        {
            
            let width:CGFloat = (self.view!.frame.size.width)
            
            
            let labelTitleText = self.products[indexPath.row].name
    
            let labelTitleWidth = self.view.frame.size.width - 106
            
            let labelTitleFont = UIFont (name: "HelveticaNeue-Bold", size: 15)
            let labelTitleSize : CGSize = Helper.sizeOfText(labelTitleText, font: labelTitleFont, width: labelTitleWidth, lineBreakMode: NSLineBreakMode.ByWordWrapping, numberOfLines:2)
            
            
            let labelDetailText = self.products[indexPath.row].detailShort
            let labelDetailWidth = self.view.frame.size.width - 106
            let labelDetailFont = UIFont (name: "HelveticaNeue-Light", size: 15)
            let labelDetailSize : CGSize = Helper.sizeOfText(labelDetailText, font: labelDetailFont, width: labelDetailWidth, lineBreakMode: NSLineBreakMode.ByWordWrapping, numberOfLines:3)
        
            print("labelTitleSize = \(labelTitleSize.height)")
             println(" \(labelDetailSize.height)")
            
            return CGSizeMake(width, 10 + labelTitleSize.height + 3 + labelDetailSize.height + 83 )
            
        }
        return CGSizeZero

    }
        
       
    // MARK: CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cellIdentifier = GridIdentifier

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
}


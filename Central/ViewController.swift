//
//  ViewController.swift
//  Central
//
//  Created by nickay on 7/9/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit
let LIMIT = 20



class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIViewControllerTransitioningDelegate{

    @IBOutlet weak var actionButton: UIButton!
    var myProducts:[MSProduct] = []
    let transition = BubbleTransition()
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myProducts.count
    }
    func  numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
        let product = myProducts[indexPath.row]
        
        cell.textLabel!.text="\(product.id)"
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
          let product = myProducts[indexPath.row] as MSProduct

        
            product.getProductRelatedMagicSearch { (products, error) -> Void in
                if products != nil
                {
                    println("\(products)")
                }
                else
                {
                    println("\(error)")
                }
            //
        }
    }
//    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell!
//    {
//        
//        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
//        return cell
//    }
    override func viewDidLoad()
    {
        
          super.viewDidLoad()
        //self.testUserSignUp()
        self.testLoginWithUserName()
        self.ProductPromotionForApp()
        
        
    }
    // MARK: TEST USER METHOD
    func testUserSignUp()
    {
      
        
        var user = CTUser()
        
        user.userName = "nicky4"
        var random = arc4random()%10000
        
       //user.email = "nicky\(random)@more.com"
        user.email = "nicky4@more.com"
        user.password = "1234"
        user.the1CardNumber = "24113456"
        user.tel="0917130646"

        
        user.signUpNewUserWithThe1Card { (user, error) -> Void in
            
            println("user =\(user)")
            println("error =\(error)")
        }

        
    }
    
    func testLoginWithUserName()
    {
        
        
        
        CTUser.logInWithUserName("nicky4@more.com", password: "1234") { (user, error) -> Void in
            println("user =\(CTUser.currentUser?.nickName)")
            println("error =\(error)")
        }
      
    
    }
    func testLogOut()
    {
        CTUser.logOut { (success, error) -> Void in
            
            if(success)
            {
                println("user \(CTUser.currentUser)")
            }
            else
            {
                println("error \(error)")
            }
        }

    }
    func testUpdate()
    {
     
        CTUser.currentUser?.nickName = "new"
        CTUser.currentUser?.update({ (success, error) -> Void in
            if(success)
            {
                 println("user =\(CTUser.currentUser?.nickName)")
            }
            else
            {
                println("error \(error)")
            }
        })
        
    }
    func testChangePassword()
    {
        CTUser.changePassword("5678", oldPassword: "1234") { (success, error) -> Void in
            
            if(success)
            {
                println("success")
            }
            else
            {
                println("error \(error)")
            }
        }
        
    }
     // MARK: TEST PRODUCT METHOD
    func testProductBestSeller()
    {
        MSProduct.getProductBestSeller { (products, error) -> Void in
            if(products != nil)
            {
             //   println("success \(products?[1])")
                self.myProducts += products!
                
                self.tableView.reloadData()
                
            }
            else
            {
                println("error \(error)")
            }        }
        
    }
   
    func ProductPromotionForApp()
    {
        MSProduct.getPromotionForMobile { (products, error) -> Void in
            
            if(products != nil)
            {
               
                  println("products \(products)")
                
                self.myProducts += products!
                
                self.tableView.reloadData()
                
            }
            else
            {
                println("error \(error)")
            }
        }
    }
    func ProductPromotionHome()
    {
        
    }
    func ProductBarcode()
    {
        
    }
    func ProductRecommend()
    {
        
    }
    func ProductRelated()
    {
        
    }
    func ProductSimilar()
    {
        
    }
    func ProductWishlist()
    {
        
    }
    func ProductDetails()
    {
        
    }
    func ProductSearch()
    {
        
    }
    func ProductbySubCategory()
    {
        
    }
    func ProductByBanner()
    {
        
    }
    func ProductGroupBySubCategory()
    {
        
    }
    func ProductOnPromotion()
    {
        
    }
    func ProductByBrand()
    {
        
    }
    func ProductGroup()
    {
        
    }
    



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pressAction(sender: UIButton)
    {
        self.testProductBestSeller()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? UIViewController {
            controller.transitioningDelegate = self
            controller.modalPresentationStyle = .Custom
        }
    }
    
    // MARK: UIViewControllerTransitioningDelegate
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .Present
        transition.startingPoint = self.actionButton.center
        transition.bubbleColor = self.actionButton.backgroundColor!
        return transition
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .Dismiss
        transition.startingPoint = self.actionButton.center
        transition.bubbleColor = self.actionButton.backgroundColor!
        return transition
    }
    
}


//
//  BrowseViewController.swift
//  Central
//
//  Created by nickay on 7/16/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class BrowseViewController: UIViewController,BarViewControllerDelegate,PPRevealSideViewControllerDelegate,MenuTableViewControllerDelegate
{
    
    var menuTableViewController:MenuTableViewController! = nil
    var productListViewController:ProductListViewController! = nil//MenuTableViewController

    
   
    @IBOutlet weak var loadingIndicatorView: DGActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("MenuTableViewController") as? MenuTableViewController
        {
            menuTableViewController = vc
        }
        menuTableViewController.delegate = self
        loadingIndicatorView.type = DGActivityIndicatorAnimationType.DoubleBounce
        loadingIndicatorView.size = 20.0
        loadingIndicatorView.tintColor = UIColor.redColor()
        loadingIndicatorView.startAnimating()
        
        MSProduct.getPromotionForMobile { (products, error) -> Void in
            
          if products != nil
          {
            self.productListViewController.products = products!
            self.productListViewController.collectionView.reloadData()
            self.productListViewController.view.fadeUp(nil)
           }
            

        }
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: -BarViewControllerDelegate
    func didPressMenuButton() {
        
        println("didPressMenuButton")
        
        self.revealSideViewController.delegate = self
        self.revealSideViewController.resetOption(PPRevealSideOptions.OptionsiOS7StatusBarFading)
        self.revealSideViewController.pushViewController(menuTableViewController, onDirection: PPRevealSideDirection.Top, withOffset: 43, animated: true) { () -> Void in
            
        }
      
    }
    // MARK: -MenuTableViewControllerDelegate
    func didSelectMenu() {
        
        self.productListViewController.view.layer.opacity = 0
        
        self.loadingIndicatorView.layer.opacity = 1
        
        
        MSProduct.getPromotionForMobile { (products, error) -> Void in
            
            if products != nil
            {
                self.productListViewController.products = products!
                self.productListViewController.collectionView.reloadData()
                
                self.loadingIndicatorView.fadeDown({ (animation, success) -> Void in
                    if(success)
                    {
                        self.productListViewController.view.fadeUp(nil)
                    }
                })

                
            }
            
            
        }

        

    }
    
    func pprevealSideViewController(controller: PPRevealSideViewController!, willPopToController centerController: UIViewController!) {
        
        
        var scale: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
        scale.fromValue = NSValue(CGPoint: CGPointMake(1, 1))
        scale.toValue = NSValue(CGPoint: CGPointMake(0.8, 0.8))
        menuTableViewController.tableView.layer.pop_addAnimation(scale, forKey: "scaleDown")
        
        var alpha: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerOpacity)
         alpha.fromValue = 1
        alpha.toValue = 0
        menuTableViewController.tableView.layer.pop_addAnimation(alpha, forKey: "fadeDown")
        
    }
//    - (void)pprevealSideViewController:(PPRevealSideViewController *)controller willPopToController:(UIViewController *)centerController
//    {
//    
//    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toBarViewController"
        {
            if let vc = segue.destinationViewController as? BarViewController
            {
                vc.delegate = self
            }
        }
        else if segue.identifier == "toProductGridViewController"
        {
            if let vc = segue.destinationViewController as? ProductListViewController
            {
                productListViewController = vc
            }

        }
        
    }


}

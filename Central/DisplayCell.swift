//
//  DisplayCell.swift
//  Central
//
//  Created by nickay on 7/16/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class DisplayCell: UICollectionViewCell {
    
    @IBOutlet weak var rootView: UIView!
    
    @IBOutlet  weak var productCellView: ProductCellView!
     private var numberOfItemsPerLine: Int = 0
    
    
    func updateLayout()
    {
        
        if(self.productCellView != nil)
        {
            
//            Helper.deley(time: 0, task: { () -> Void in
//                
//                          self.productCellView.updateLayout(self.numberOfItemsPerLine)
//            })
             self.productCellView.updateLayout(self.numberOfItemsPerLine)

            

        }
    
              //self.productCellView.updateLayout(self.numberOfItemsPerLine)
        

       
    }
    func setNumberOfItemsPerLine(numberOfItemsPerLine: Int)
    {
        if(self.numberOfItemsPerLine != numberOfItemsPerLine)
        {
            self.numberOfItemsPerLine = numberOfItemsPerLine
            self.updateLayout()
        }
    }
    
}

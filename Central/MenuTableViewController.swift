//
//  MenuTableViewController.swift
//  Central
//
//  Created by nickay on 7/16/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {

     weak var delegate:MenuTableViewControllerDelegate? = nil
     var numberOfSubDepartMent = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if section == 0
        {
            return numberOfSubDepartMent
        }
        else if section == 1
        {
            return 3
        }
        else if section == 2
        {
            return 2
        }
        else if section == 3
        {
            return 2
        }
        return 0
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(section == 0)
        {
              return 44
        }
      
        
        return 20
    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var headerView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
        headerView.backgroundColor = UIColor.blackColor()
        var titleHeaderLabel = UILabel(frame: headerView.frame)
        headerView.addSubview(titleHeaderLabel)
        titleHeaderLabel.textColor = UIColor.whiteColor()
        titleHeaderLabel.textAlignment = NSTextAlignment.Center
        var boldHelveticaFont = UIFont(name: "Helvetica Neue", size: 17)?.fontDescriptor().fontDescriptorWithSymbolicTraits(UIFontDescriptorSymbolicTraits.TraitBold)
        titleHeaderLabel.font = UIFont(descriptor: boldHelveticaFont!, size: 17)
        var actionButton = UIButton(frame: headerView.frame)
        actionButton.backgroundColor = UIColor.clearColor()
        
        actionButton.addTarget(self, action: "didPressDepartmentSection:", forControlEvents: UIControlEvents.TouchUpInside)
        
         headerView.addSubview(actionButton)
        if(section == 0)
        {
            titleHeaderLabel.text = "Departments"
            actionButton.hidden = false
 
        }
        else
        {
            titleHeaderLabel.text = ""
            actionButton.hidden = true
        }
        
        
        
        return headerView
    }
    func didPressDepartmentSection(sender:UIButton)
    {
        if numberOfSubDepartMent == 0
        {
            numberOfSubDepartMent = 5
        }
        else
        {
            numberOfSubDepartMent = 0
        }
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        println("\(self.delegate)")
        self.delegate?.didSelectMenu()
        self.revealSideViewController?.popViewControllerAnimated(true)

    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)
//        var titleLabel = cell.contentView.subviews[0] as! UILabel
//        titleLabel.text = "YOLO"
        return cell
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
    
        var scale: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
        scale.fromValue = NSValue(CGPoint: CGPointMake(0.8, 0.8))
        scale.toValue = NSValue(CGPoint: CGPointMake(1, 1))
        self.tableView.layer.pop_addAnimation(scale, forKey: "scaleUp")
        
        var alpha: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerOpacity)
        alpha.fromValue = 0
        alpha.toValue = 1
        self.tableView.layer.pop_addAnimation(alpha, forKey: "fadeUp")
        
    
        
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        var scale: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerScaleXY)
       
        scale.toValue = NSValue(CGPoint: CGPointMake(self.view.frame.size.width/self.tableView.frame.size.width, self.view.frame.size.height/self.tableView.frame.size.height))
        self.tableView.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)
        self.tableView.layer.pop_addAnimation(scale, forKey: "scaleUp")
        
        var alpha: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerOpacity)
        alpha.toValue = 1
        self.tableView.layer.pop_addAnimation(alpha, forKey: "fadeUp")
        
        

    }
}
protocol MenuTableViewControllerDelegate :NSObjectProtocol
{
    func didSelectMenu()
}


//
//  UIView+Animation.swift
//  Central
//
//  Created by nickay on 7/17/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

extension UIView
{
    func fadeUp(completionHandler: ((animation:POPAnimation!, success:Bool) -> Void)?)
    {
        var alpha: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerOpacity)
        alpha.toValue = 1
        alpha.completionBlock = completionHandler
        self.layer.pop_addAnimation(alpha, forKey: "fadeUp")
        
    }
    func fadeDown(completionHandler: ((animation:POPAnimation!, success:Bool) -> Void)?)
    {
        var alpha: POPSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerOpacity)
        alpha.toValue = 0
        
    
         alpha.completionBlock = completionHandler
        self.layer.pop_addAnimation(alpha, forKey: "fadeDown")
        
    }
}
//
//  MSCartItem.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/8/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSCartItem: MSObject {
   
    var product: MSProduct!
    var dateAdded: NSDate = NSDate()

    var quantity: Int = 1
    
    func subTotal()-> Float {
        return self.product.price * Float(self.quantity)
    }
    
    
    func updateInCart(cart:MSCart,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        APIService.manager.updateCartItemInCart(cart, cartItem: self, completionHandler: completionHandler)
    }
   

}

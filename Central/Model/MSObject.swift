//
//  MSModel.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSObject: NSObject
{
    let keyObjectId = "Id"
    internal var _id: String!
    var id: String { get { return self._id} }
    
    var isDataAvaliable = false
    
    var remark: String!
    
    override init() {
        super.init()
    }
    
    init(info: JSON) {
        super.init()
        self.config(info)
    }
    
    init(id: String) {
        super.init()
        self._id = id
    }
    
    func save() {
        
    }
    
    func load() {
        
    }
    
    
    func config(info:JSON)
    {
        _id = info[keyObjectId].stringValue
    }
    
}

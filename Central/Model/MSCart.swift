//
//  MSCart.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSCart: MSObject {
    
    class func currentCart()-> MSCart {
        return MSCart()
    }
   
    var items: [MSCartItem] = []
    
    var discountPercent: Float = 0
    var coupon: MSCoupon?
    
    func displaySubtotalString()-> String {
        return ""
    }
    
    func addItem(product: MSProduct, quantity: Int) {
        
    }
    
    func removeItem(item: MSCartItem) {
        
    }
    
    func removeItemAtIndex(index: Int) {
        
    }
    
    func clearCart() {
        
    }
    
    func updateItem(item: MSCartItem, quantity: Int) {
        
    }
    
    func updateAtIndex(index: Int, quantity: Int) {
        
    }
    func getCartItemListInCart (completionHandler: ((cartItems:[MSCartItem]?, error:NSError?) -> Void)?)
    {
        
       APIService.manager.getCartItemListInCart(self, completionHandler: completionHandler)
    }
    func checkOutCart(coupons:[MSCoupon]?,customObjects:AnyObject? ,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        APIService.manager.checkoutCart(self, coupons: coupons,customObjects: customObjects, completionHandler: completionHandler)
    }
}

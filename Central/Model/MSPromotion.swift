//
//  MSPromotion.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/8/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSPromotion: MSObject {
    var image: MSImage?
    var openURL: NSURL? // http:// morestu://privilege/1, morestu://productList/category/20
    
    var detail: String?
    var tagline: String?
}

//
//  MSCategory.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSCategory: MSObject {
   
    var name: String!
    var detail: String!
    var tagline: String!
    
    var parent: MSCategory?
    
    var iconImage: MSImage?
    var bannerImage: MSImage?
    
    var hasSubCategory: Bool = false
    var subCategory: [MSCategory]?
    
    class func getDepartments(sortBy:String?,completionHandler: ((user:[MSCategory]?, error:NSError?) -> Void)?)
    {
        
        
    }
    class func getSubDepartmentInDepartment(department:MSCategory,sortBy:String?,completionHandler: ((user:[MSCategory]?, error:NSError?) -> Void)?)
    {
        
        
    }
    
}

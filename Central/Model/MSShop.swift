//
//  MSShop.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/9/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSShop: MSObject {
    
    var name: String!
    var shortLocation: String?
    
    weak var store: MSStore!
}
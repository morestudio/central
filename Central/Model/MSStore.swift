//
//  MSStore.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/8/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSStore: MSAddress
{

    let keyName = "name"
    let keyFaxNumber = "faxNumber"
    let keyGoogleMapURL = "googleMapURL"
    let keyMapImages = "mapImage"
    let keyShops = "shops"
    let keyOpeningHours = "openingHours"
    
    
    
    var name: String!
    
    var faxNumber: String?
    var googleMapURL: NSURL?
    var mapImage: MSImage?
    
    var shops: [MSShop] = []
    
    var openingHours: AnyObject!
    
    func distanceWithUserLocation(location: AnyObject)-> Double {
        return 0
    }
    
   
    
}

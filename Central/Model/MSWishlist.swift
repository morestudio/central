//
//  Wishlist.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/9/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSWishlist: MSObject
{
    let keyMSWishlistItem = "MSWishlistItem"
    

    class func currentCart()-> MSCart {
        return MSCart()
    }
    
    var items: [MSWishlistItem] = []
 
    func addItem(product: MSProduct, quantity: Int = 1) {
        
    }
    
    func removeItem(item: MSCartItem) {
        
    }
    
    func removeItemAtIndex(index: Int) {
        
    }
    
    func clearWishlist() {
        
    }
    
    func updateItem(item: MSCartItem, quantity: Int) {
        
    }
    
    func updateAtIndex(index: Int, quantity: Int) {
        
    }
}

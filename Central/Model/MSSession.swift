//
//  MSSession.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSSession: MSObject
{
    private var token: String?
    private var user: MSUser?
    private var expireDate: NSDate?
    static var _session:MSSession?
    
    //// static _currentSession: MSSession
    
//    class func session()->MSSession
//    {
//        if _session == nil
//        {
//            _session = MSSession()
//        }
//        return _session!
//    }
    
    class var session: MSSession {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: MSSession? = nil
        }
        dispatch_once(&Static.onceToken, { () -> Void in
            Static.instance = MSSession()
        })
        return Static.instance!
    }
    
    override init() {
        super.init()
    }
    
    init(token: String?, user: MSUser?) {
        super.init()
        MSSession.session.token = token
        MSSession.session.user = user
        self.token = token
        self.user = user
        
    }
    
    func sessionToken()-> String? {
        
        
        return self.token
    }
    
    func currentUser()-> MSUser? {
        return self.user
    }
    
    func sessionExpireDate()-> NSDate? {
        return self.expireDate
    }
    
    func clearSession() {
        self.token = nil
        self.user = nil
    }
    
    class func currentSession()-> Self? {
        return nil
    }
    
    func config(params: AnyObject) {
        // self._currentSession. tok 
    }
    
}

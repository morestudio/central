//
//  MSOrder.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/9/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

enum MSOrderStatus {
    case Normal
}

class MSOrder: MSObject {
    var items: [MSCartItem] = []
    var createDate: NSDate!
    var paymentMethod: MSPaymentMethod!
    var status: MSOrderStatus = .Normal
    var owner: MSUser?
    
    
}

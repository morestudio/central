//
//  CTAPIService.swift
//  Central
//
//  Created by nickay on 7/13/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class CTAPIService: APIService {
   
    
    override  class var manager: CTAPIService {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: CTAPIService? = nil
        }
        dispatch_once(&Static.onceToken, { () -> Void in
            Static.instance = CTAPIService()
        })
        return Static.instance!
    }
    

    
     func userSignUpWithThe1Card(user: CTUser, completionHandler: ((user: CTUser?, error: NSError?) -> Void)?)
     {
        var params: [String: AnyObject]? = [:]
        params?["Email"]=user.email
        params?["Password"]=user.password
        params?["Mobile"]=user.tel
        params?["The1CardNumber"] = user.the1CardNumber
        
        
        let request = self.request(APIInfo.UserSignUp, params: params)
        
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(user: nil, error: error)
            } else
            {
                var returnUser = CTUser(info: json)
                completionHandler?(user: returnUser, error: nil)
            }
        }

    }
    
}

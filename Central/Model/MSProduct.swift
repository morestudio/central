//
//  MSProduct.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

struct MSProductBadge {
    var title: String
    var backgroundColor: UIColor
}

enum MSProductStatus: Int {
    case Normal
}


// *DON'T FOR GET TO CONFIG KEY NAMES //
// ====================================//
let keyName = "Name"
let keyDetail = "Description"
let keyDetailShort = "DescriptionShort"
let keytagline = "tagLine"

let keythumbnailImage = "ImageThumbUrl"
let keyNormalImage = "ImageSmallUrl"
let keyLargeImage = "ImageLargeUrl"
let keyZoomImage = "ImageZoomUrl"

let keythumbnailImages = "thumbnailImages"
let keyNormalImages = "GalleryImages"
let keyLargeImages = "largeImages"
let keyPrice = "DisplayPrice"
let keyDiscountPercent = "DiscountPercent"
let keyStatus = "ProductStatus"
let keyStockQuntity = "stockQuntity"
let keyClassifierName = "Unit"
let keyRating = "NumOfRating"
let keyTotalRateParticipants = "totalRateParticipants"
let keyBadges = "badges"
let keyBrandId = "BrandId"
let keyBrandName = "BrandName"
let keyDeptId = "DeptId"
let keyCatId = "pCatId"
let keySubCatId = "pSubCatID"





class MSProduct: MSObject
{
    var name : String!
    var detail :String!
    var detailShort :String!
    var tagline :String!
    
    var thumbnailImage : MSImage?
    var normalImage : MSImage?
    var largeImage : MSImage?
    var zoomImage : MSImage?
    
    var thumbnailImages : [MSImage]?
    var normalImages : [MSImage]?
    var largeImages : [MSImage]?
    var price: Float = 0
    var discountPercent: Float = 0
    var status:String!
    var stockQuntity: Int = 0
    var classifierName: String? //ลักษณนาม
    var rating : Float = 0
    var totalRateParticipants : Int = 0
    var badges: [MSProductBadge] = []
    var brandId :String!
    var brandName :String!
    var deptId :String!
    var catId :String!
    var subCatId :String!
   

    
    func displayPriceText()-> String {
        return "\(price * (1 -  discountPercent))"
    }
    
    func displayStatusText()-> String {
        return ""
    }
    override func config(info: JSON)
    {
        super.config(info)
        self.name = info[keyName].stringValue
        self.detail = info[keyDetail].stringValue
        self.detailShort = info[keyDetailShort].stringValue
        self.tagline = info[keytagline].stringValue
        
        
      
        self.thumbnailImage = MSImage(urlString:"\( APIService.manager.ROOT_IMAGE_URL)\(info[keythumbnailImage].stringValue)")
        self.normalImage =  MSImage(urlString: "\( APIService.manager.ROOT_IMAGE_URL)\(info[keyNormalImage].stringValue)")
        self.largeImage = MSImage(urlString: "\( APIService.manager.ROOT_IMAGE_URL)\(info[keyLargeImage].stringValue)")
        self.zoomImage =   MSImage(urlString: "\( APIService.manager.ROOT_IMAGE_URL)\(info[keyZoomImage].stringValue)")
        
//        self.thumbnailImages : [MSImage]?
//        self.normalImages : [MSImage]?
//        self.largeImages : [MSImage]?
        
        self.price = info[keyPrice].floatValue
        self.discountPercent = info[keyDiscountPercent].floatValue
        self.status = info[keyStatus].stringValue
        self.stockQuntity  = info[keyDiscountPercent].intValue
        self.classifierName = info[keyClassifierName].stringValue//ลักษณนาม
        self.rating = info[keyRating].floatValue

        self.totalRateParticipants = info[keyTotalRateParticipants].intValue
        //self.badges: [MSProductBadge] = []
        
        self.brandId = info[keyBrandId].stringValue
        self.brandName = info[keyBrandName].stringValue
        self.deptId = info[keyDeptId].stringValue
        self.catId  = info[keyCatId].stringValue
        self.subCatId  = info[keySubCatId].stringValue
        
        /*
        
        if let name = object[keyName] as? String
        {
            self.name = name
        }
        
        if let detail = object[keyDetail] as? String
        {
            self.detail = detail
        }
        
        if let tagline = object[keytagline] as? String
        {
            self.tagline = tagline
        }
        
        if let thumbnailImagesURLArray = object[keythumbnailImages] as? [String]
        {
            var returnArray:[MSImage] = []
            
            
            for imageURL in thumbnailImagesURLArray
            {
                var msImage = MSImage ()
                msImage.url = NSURL(string: imageURL)
                returnArray.append(msImage)
            }
            
            self.thumbnailImages = returnArray
        }
        
        if let normalImagesArray = object[keyNormalImages] as? [String]
        {
            var returnArray:[MSImage] = []
            
            
            for imageURL in normalImagesArray
            {
                var msImage = MSImage ()
                msImage.url = NSURL(string: imageURL)
                returnArray.append(msImage)
            }
            
            self.normalImages = returnArray
        }
        
        
        if let largeImagesArray = object[keyLargeImages] as? [String]
        {
            var returnArray:[MSImage] = []
            
            
            for imageURL in largeImagesArray
            {
                var msImage = MSImage ()
                msImage.url = NSURL(string: imageURL)
                returnArray.append(msImage)
            }
            
            self.largeImages = returnArray
        }
        
        
        if let price = object[keyPrice] as? Float
        {
            self.price = price
        }
        
        
        if let discountPercent = object[keyDiscountPercent] as? Float
        {
            self.discountPercent = discountPercent
        }
        
        
        
        if let status = object[keyStatus] as? Int
        {
            self.status = MSProductStatus(rawValue: status)
        }
        
        if let stockQuntity = object[keyStockQuntity] as? Int
        {
            self.stockQuntity = stockQuntity
        }
        
        if let classifierName = object[keyClassifierName] as? String
        {
            self.classifierName = classifierName
        }
        
        if let rating = object[keyRating] as? Float
        {
            self.rating = rating
        }
        
        if let totalRateParticipants = object[keyTotalRateParticipants] as? Int
        {
            self.totalRateParticipants = totalRateParticipants
        }
        
        //        if let badge = object["badges"] as? [String:AnyObject]
        //        {
        //            if let badgeTitle = badge["title"] as? String
        //            {
        //                 var _badge = MSProductBadge(title: badgeTitle, backgroundColor: UIColor.whiteColor())
        //            }
        //
        //        }
        
        if let brand = object[keyBrand] as? [String:AnyObject]
        {
            if let brandID = brand[keyID] as? String
            {
                var _brand = MSBrand(id: brandID)
            }
        }*/
        
    }
    
 
    
    // MARK: FUNCTION
    
    class func getProductBestSeller(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        APIService.manager.getProductBestSeller(completionHandler)
    }
    func getProductRelatedMagicSearch(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        APIService.manager.getProductRelatedMagicSearch(self.id, completionHandler: completionHandler)
    }
    class func getProductDetail(objectID:String,completionHandler: ((product:MSProduct?, error:NSError?) -> Void)?)
    {
        APIService.manager.getProductDetail(objectID, completionHandler: completionHandler)
    }
    class func getProductsFromPromotion(promotion:MSPromotion?,sort:String?,offset:Int,limit:Int,completionHandler: ((user:[MSProduct]?, error:NSError?) -> Void)?)
    {
        APIService.manager.getProductPromotionHome(completionHandler)
        
    }
    //getPromotionForMobile
    class func getPromotionForMobile(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
         APIService.manager.getPromotionForMobile(completionHandler)
    }
    class func getProductPromotionHome(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        APIService.manager.getProductPromotionHome(completionHandler)
    }
    
    class func getProductsFromRecentlyView(completionHandler: ((user:[MSProduct]?, error:NSError?) -> Void)?)
    {
        
        
    }
    class func getProductsFromBrand(brand:MSBrand, sort:String?,offset:Int,limit:Int,completionHandler: ((user:[MSProduct]?, error:NSError?) -> Void)?)
    {
        
        
    }
    func addProductToWishList (completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
        APIService.manager.addProductToWishList(self, completionHandler: completionHandler)
    }
    
    func shareProduct(completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
        
    }
   
    


    
}

//
//  ImageModel.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSImage: MSObject {
    
    var imageName: String?
    var url: NSURL?
    var data: NSData?
    var imageCache:NSCache!
    
    func getImage(imageView:UIImageView, completionHandler: ((image:UIImage?, error:NSError?) -> Void)?)
    {
        
        if self.imageName != nil
        {
            completionHandler?(image:UIImage(named: self.imageName!) , error:nil)
        }
        else if self.data != nil
        {

            completionHandler?(image:UIImage(data: self.data!) , error:nil)
        }
        else if MSImage.manager.imageCache.objectForKey(self.url!) != nil
        {
            if let image = MSImage.manager.imageCache.objectForKey(self.url!) as? UIImage
            {
               completionHandler?(image:image , error:nil)
            }
            else
            {
                println("ใส่ข้อมูลใน cache ผิด")
            }
        }
        else
        {
            
            println("self.url = \(self.url)")
            imageView.sd_setImageWithURL(self.url, completed: { (image, error, type:SDImageCacheType, url) -> Void in
                if image != nil
                {
                    MSImage.manager.imageCache.setObject(image, forKey: self.url!)

                }
            })
        }
        
       // return nil
    }
    
    init(urlString: String) {
        super.init()
        self.url = NSURL(string: urlString)
        
    }
    
    class var manager: MSImage
    {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: MSImage? = nil
        }
        dispatch_once(&Static.onceToken, { () -> Void in
            Static.instance = MSImage(urlString:"")
            Static.instance?.imageCache = NSCache()
        })
        return Static.instance!
    }


}

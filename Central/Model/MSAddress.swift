//
//  MSAddress.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSAddress: MSObject {
    
    var recipientName: String = ""
    var addressLine1: String = "" // เลขที่ ห้อง ชั้น อาคาร ถนน
    var addressLine2: String = ""
    var subDistric: String = "" // แขวง / ตำบล
    var distric: String = "" // เขต / อำเภอ
    var city: String = "" // จังหวัด
    var country: String = ""
    private var postcode: String = ""
    
    private var phoneNumber: String = ""
    
    var note: String = ""
    
    var latitude: Double = 0
    var longitude: Double = 0
    
    func fullAddress()-> String {
        self.addressLine1 = ""
        
        return ""
    }
    
    func setPhoneNumber(number: String)-> Bool {
        // verify
        self.phoneNumber = number
        return true
    }
    
    func setPostcode(postcode: String)-> Bool {
        
        self.postcode = postcode
        return true
    }
    
    
    //TODO: - varidate set properties
}

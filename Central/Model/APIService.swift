//
//  APIService.swift
//  Central
//
//  Created by nickay on 7/9/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit





class APIService: NSObject {
   
    private let ENDPOINT_KEY = "endpoint"
    let ROOT_URL = "http://api.officemate.co.th"
    let ROOT_IMAGE_URL = "http://officemate.co.th"
    
    override init() {
        super.init()
       
    }
    
     // MARK: PRODUCT HELPER
    
    class var manager: APIService {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: APIService? = nil
        }
        dispatch_once(&Static.onceToken, { () -> Void in
            Static.instance = APIService()
        })
        return Static.instance!
    }
    

    
    func smartURLForString(str: String) -> NSURL? {
        var result: NSURL? = nil
        var trimmedStr: NSString?
        var schemeMarkerRange: NSRange
        var scheme: NSString?
        
        trimmedStr = str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if trimmedStr != nil && trimmedStr?.length != 0 {
            schemeMarkerRange = trimmedStr!.rangeOfString("://")
            if schemeMarkerRange.location == NSNotFound {
                result = NSURL(string: "http://\(trimmedStr)")
            } else {
                scheme = trimmedStr!.substringWithRange(NSMakeRange(0, schemeMarkerRange.location))
                assert(scheme != nil, "scheme not found")
                
                if scheme!.compare("http", options: NSStringCompareOptions.CaseInsensitiveSearch) == NSComparisonResult.OrderedSame {
                    result = NSURL(string: trimmedStr! as String)
                } else {
                    // it looks like this is some unsupported URL scheme
                }
            }
        }
        
        return result
    }
    
    private func HTTPRoute(endpoint:String) -> String {
        if let url = smartURLForString("\(ROOT_URL)\(endpoint)") {
            return url.absoluteString!
        }
        return NSURL(string: endpoint, relativeToURL: NSURL(string: ROOT_URL))!.absoluteString!
    }
    
    func request(endpoint: String, method: Method, params: [String: AnyObject]?) -> Request {
        let apiURL: String
        if let encodeEndpoint = endpoint.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) {
            apiURL = HTTPRoute(encodeEndpoint)
        } else {
            apiURL = HTTPRoute(endpoint)
        }
        
        Manager.sharedInstance.session.configuration.timeoutIntervalForResource = 20
        var request =  Manager.sharedInstance.request(method, apiURL, parameters: params)
        request.validate(statusCode: 200..<300)
        return request
    }
    
    func request(apiInfo: APIInfo, params: [String: AnyObject]?)-> Request
    {
       
        var apiURL = HTTPRoute(apiInfo.endPoint)
        
        if let endpoint = params?[ENDPOINT_KEY] as? String {
            if let safeEndPoint = endpoint.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) {
                apiURL = apiURL.stringByAppendingString(safeEndPoint)
            } else {
                apiURL = apiURL.stringByAppendingString(endpoint)
            }
            if let url = smartURLForString(apiURL) {
                var request =  Manager.sharedInstance.request(apiInfo.method, url.absoluteString!, parameters: nil)
                return request
            }
        }

        
        return self.request(apiInfo.endPoint, method: apiInfo.method, params: params)
    }

    
    // MARK: USER METHOD
    
    func userSignUp (user: MSUser, completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        params?["Email"]=user.email
        params?["Password"]=user.email

        
        let request = self.request(APIInfo.UserSignUp, params: params)
        
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(user: nil, error: error)
            } else
            {
                var returnUser = MSUser(info: json)
                completionHandler?(user: returnUser, error: nil)
            }
        }
    }
    func userLogIn (user:MSUser, completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = [:]
        
        params?["UserName"] = user.userName
        params?["Password"] = user.password
        

        
        let request = self.request(APIInfo.UserLogIn, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(user: nil, error: error)
            } else
            {
                var returnUser = MSUser(info: json["User"])
                MSSession(token: json["TokenId"].stringValue, user: returnUser)
                completionHandler?(user: returnUser, error: nil)
            }
        }
        
    }
    func userLogOut (user:MSUser, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = [:]
        params?["tokenId"] = MSSession.session.sessionToken()
        
        
        let request = self.request(APIInfo.UserLogOut, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {
                completionHandler?(success: json["IsSuccess"].boolValue , error: nil)
            }
          
        }

    }
    func userForgotPassword (completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
              let request = self.request(APIInfo.UserForgotPassword, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {

                completionHandler?(success: true , error: nil)
            }
        }

    }
    func userChangePassword (newPassword:String,oldPassword:String, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = [:]
        params?["tokenId"] = MSSession.session.sessionToken()
        params?["old_password"] = oldPassword
        params?["new_password"] = newPassword
        
        let request = self.request(APIInfo.UserChangePassword, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                if(!json["IsSuccess"].boolValue)
                {
                    var error = NSError(domain: request.URLString, code: json["ErrorCode"].intValue, userInfo: ["ErrorCode":json["ErrorCode"].stringValue,"ErrorMessage":json["ErrorMessage"].stringValue])
                    completionHandler?(success: false , error: error)
                    
                }
                completionHandler?(success: true , error: nil)
            }
        }

    }
    func getUserInfo (objectId:String ,completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.UserInfo, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(user: nil , error: error)
            } else
            {
                
                var returnUser = MSUser(info: json)

                
                completionHandler?(user: returnUser , error: nil)
            }
        }
        
    }
    func updateUserInfo  (user:MSUser,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
        var params: [String: AnyObject]? = [:]
        params?["tokenId"] = MSSession.session.sessionToken()
        params?["DisplayName"] = user.nickName
        params?["FullName"] = user.fullName
        params?["MobileNumber"] = user.tel
       
        
        if let ctUser = user as? CTUser
        {
             params?["T1CardNo"] = ctUser.the1CardNumber
        }

        
        
        
        let request = self.request(APIInfo.UserUpdateInfo, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                if(!json["IsSuccess"].boolValue)
                {
                   
                    
                    var error = NSError(domain: request.URLString, code: 123, userInfo: ["ErrorCode":json["ErrorCode"].stringValue,"ErrorMessage":json["ErrorMessage"].stringValue])
                    completionHandler?(success: false , error: error)
                    
                }
                completionHandler?(success: true , error: nil)
            }
        }

    }
     // MARK: SHIPPING_ADDRESS METHOD
    
    func getShippingAddressList(completionHandler: ((shippingAddressList:[MSAddress]?,error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressList, params: params)
        
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil {
                
                completionHandler?(shippingAddressList: nil, error: nil)
            } else {
                completionHandler?(shippingAddressList: nil, error: error)
            }
        }
    }
    
    func getShippingAddressDetail(objectID:String!,completionHandler: ((address:MSAddress?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(address: nil , error: error)
            } else
            {
                
                var returnAddress = MSAddress(info:json)
               
                
                completionHandler?(address: returnAddress , error: nil)
            }
        }

    }
    func createNewShippingAddress( shippingAddress:MSAddress , completionHandler: ((address:MSAddress?, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressCreateNew, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(address: nil , error: error)
            } else
            {
                
                var returnAddress = MSAddress(info:json)
               
                
                completionHandler?(address: returnAddress , error: nil)
            }
        }
        
    }
    func updateShippingAddress(address:MSAddress,completionHandler: ((address:MSAddress?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressUpdate, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(address: nil , error: error)
            } else
            {
                
                var returnAddress = MSAddress(info: json)
        
                
                completionHandler?(address: returnAddress , error: nil)
            }
        }
    }
    func setDefaultShippingAddress (address:MSAddress, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressSetDefault, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
  
                completionHandler?(success: true , error: nil)
            }
        }

    }
    func deleteShippingAddress (address:MSAddress , completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShippingAddressDelete, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                completionHandler?(success: true , error: nil)
            }
        }
    }
    // MARK: PRODUCT METHOD
    
    
     func getProductBestSeller(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
     {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductBestSellerList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            }
            else
            {
                
                var returnProducts:[MSProduct] = []
                
                for rawProduct in json.arrayValue
                {
                    var product = MSProduct(info: rawProduct)
                    returnProducts.append(product)
    
                }
                
                completionHandler?(products: returnProducts , error: nil)
            }
        }
        
    }
    func getPromotionForMobile(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.PromotionForMobile, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            }
            else
            {
                
                var returnProducts:[MSProduct] = []
                
                for rawProduct in json["ProductData"].arrayValue
                {
                    var product = MSProduct(info: rawProduct)
                    returnProducts.append(product)
                    
                }
                
                completionHandler?(products: returnProducts , error: nil)
            }
        }
        
    }

    func getProductPromotionHome(completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductPromotionHome, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            }
            else
            {
                
                var returnProducts:[MSProduct] = []
                
                for rawProduct in json.arrayValue
                {
                    var product = MSProduct(info: rawProduct)
                    returnProducts.append(product)
                    
                }
                
                completionHandler?(products: returnProducts , error: nil)
            }
        }
        
    }

    func getProductRelatedMagicSearch(objectID:String, completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)

    {
        let params = [ENDPOINT_KEY: objectID]
        let request = self.request(APIInfo.ProductBestSellerList, params: params)

        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            }
            else
            {
                
                var returnProducts:[MSProduct] = []
                
                for rawProduct in json.arrayValue
                {
                    var product = MSProduct(info: rawProduct)
                    returnProducts.append(product)
                    
                }
                
                completionHandler?(products: returnProducts , error: nil)
            }
        }

    }
    
    func getProductDetail(objectID:String,completionHandler: ((product:MSProduct?, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductDetail, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            if error != nil
            {
                
                completionHandler?(product: nil , error: error)
            } else
            {
                
                var returnProduct = MSProduct(info:json)
         
                
                completionHandler?(product: returnProduct , error: nil)
            }
        }
        
    }
    func getProductInCatagory(catagory:MSCategory,completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductDetail, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
           
            if error != nil
            {
                
                completionHandler?(products: nil , error: error)
            } else
            {
                
                var returnProducts:[MSProduct] = []
                
                for rawProduct in json.arrayValue
                {
                    var product = MSProduct(info: rawProduct)
    
                }
                
                completionHandler?(products: returnProducts , error: nil)
            }
        }

    }
    
    func getProductRelatedList(product:MSProduct!,completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductRelatedList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(products: nil , error: error)
            } else
            {
                
                var returnObjects:[MSProduct] = []
                
                for raw in json.arrayValue
                {
                    var object = MSProduct(info: raw)
                    returnObjects.append(object)
                    
                }
                
                completionHandler?(products: returnObjects , error: nil)
            }
        }

    }
    func getProductSimilarList(product:MSProduct,completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ProductSimilarList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(products: nil , error: error)
            } else
            {
                
                var returnObjects:[MSProduct] = []
                
                for raw in json.arrayValue
                {
                    var object = MSProduct(info: raw)
                     returnObjects.append(object)
                }
                
                completionHandler?(products: returnObjects , error: nil)
            }
        }
    }
    func addProductToWishList(product:MSProduct,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.WishAddProduct, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                
                completionHandler?(success: true , error: nil)
            }
        }

    }
    
    // MARK: CATAGORY METHOD
    
    func getCategoryList (completionHandler: ((catagories:[MSCategory]?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CategoryList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(catagories: nil , error: error)
            } else
            {
                
                var returnObjects:[MSCategory] = []
                
                for raw in json.arrayValue
                {
                    var object = MSCategory(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(catagories: returnObjects , error: nil)
            }
        }

        
    }
    func getCategoryDetail (objectID:String!,completionHandler: ((catagory:MSCategory?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CategoryDetail, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(catagory: nil , error: error)
            } else
            {
                
                var returnObject = MSCategory(info: json)
             
                
                completionHandler?(catagory: returnObject , error: nil)
            }
        }

    }
    
    // MARK: Cart METHOD
  
    func getCartItemListInCart (cart:MSCart , completionHandler: ((cartItems:[MSCartItem]?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CartProductList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(cartItems: nil , error: error)
            } else
            {
                
                var returnObjects:[MSCartItem] = []
                
                for raw in json.arrayValue
                {
                    var object = MSCartItem(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(cartItems: returnObjects , error: nil)
            }
        }
    }
    func addProductToCart (cart:MSCart, product:MSProduct, unit:Int, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CartAddProduct, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }

    }
    func removeProductFromCart (cart:MSCart, cartItem:MSCartItem, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CartRemoveProduct, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }
    }
    func updateCartItemInCart (cart:MSCart, cartItem:MSCartItem, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CartUpdateProduct, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }

    }
    func checkoutCart (cart:MSCart, coupons:[MSCoupon]?,customObjects:AnyObject?, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.CartCheckOut, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }
        
    }
    func clearAllCartItem (cart:MSCart, completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil

        
        let request = self.request(APIInfo.CartClearAllProduct, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }
    }
    
    // MARK: STORE & SHOP METHOD
    
    func getStoreList(completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.StoresList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {
                
                completionHandler?(success: true , error: nil)
            }
        }
    }
    func getStoreDetail(objectID:String!,completionHandler: ((store:MSStore?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.StoreDetail, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(store: nil , error: error)
            } else
            {
                
                var returnObject = MSStore(info: json)
                
                completionHandler?(store: returnObject , error: nil)
            }
        }

    }
    func getShopListInStore (store:MSStore ,completionHandler: ((shops:[MSShop]?, error:NSError?) -> Void)?)
    {
        let params: [String: AnyObject]? = nil
        let request = self.request(APIInfo.ShopsList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                
                completionHandler?(shops: nil , error: error)
            } else
            {
                
                var returnObjects:[MSShop] = []
                
                for raw in json.arrayValue
                {
                    var object = MSShop(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(shops: returnObjects , error: nil)
            }
        }

    }
    func getShopDetail (objectID:String!,completionHandler: ((shop:MSShop?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.ShopDetail, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(shop: nil , error: error)
            } else
            {
                
                var returnObject = MSShop(info: json)
                
                completionHandler?(shop: returnObject , error: nil)
            }
        }
    }
    
    // MARK: SEARCH
    
    func searchProductByName (name:String!,completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.SearchProductByName, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            } else
            {
                
                var returnObjects:[MSProduct] = []
                
                for raw in json.arrayValue
                {
                    var object = MSProduct(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(products: returnObjects , error: nil)
            }
        }

    }
    func searchProductByBarcode (barcode:String!,completionHandler: ((products:[MSProduct]?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.SearchProductByBarcode, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(products: nil , error: error)
            } else
            {
                
                var returnObjects:[MSProduct] = []
                
                for raw in json.arrayValue
                {
                    var object = MSProduct(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(products: returnObjects , error: nil)
            }
        }
    }
    
    // MARK: PROMOTION
    
    func getPromotionList (completionHandler: ((promotions:[MSPromotion]?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.PromotionList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(promotions: nil , error: error)
            } else
            {
                
                var returnObjects:[MSPromotion] = []
                
                for raw in json.arrayValue
                {
                    var object = MSPromotion(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(promotions: returnObjects , error: nil)
            }
        }

    }
    
    // MARK: PAYMENT
    
    func getPaymentMethodsList (completionHandler: ((paymentMethods:[MSPaymentMethod]?, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.PaymentMethodsList, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(paymentMethods: nil , error: error)
            } else
            {
                
                var returnObjects:[MSPaymentMethod] = []
                
                for raw in json.arrayValue
                {
                    var object = MSPaymentMethod(info: raw)
                    returnObjects.append(object)
                }
                
                completionHandler?(paymentMethods: returnObjects , error: nil)
            }
        }

    }
    
    // MARK: ORDER
    
//    func getOrderHistoryList (completionHandler: ((shop:[MSHistory]?, error:NSError?) -> Void)?)
//    {
//        
//    }
    
    func createNewOrder (order:MSOrder ,completionHandler: ((order:MSOrder?, error:NSError?) -> Void)? )
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.OrderCreateNew, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(order: nil , error: error)
            } else
            {
     
                var returnObject = MSOrder (info: json)
                
                completionHandler?(order: returnObject , error: nil)
            }
        }

    }
    func repurchaseOrder (order:MSOrder!,completionHandler: ((success:Bool, error:NSError?) -> Void)? )
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.OrderRepurchase, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {
                
                var returnObject = MSOrder (info: json)
                
                completionHandler?(success: true , error: nil)
            }
        }

    }
    
     // MARK: COUPON
    
    
    func addCouponToCart (coupon:MSCoupon!,cart:MSCart!,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        var params: [String: AnyObject]? = nil
        
        
        let request = self.request(APIInfo.CartAddCoupon, params: params)
        request.responseSwiftyJSON { (request, respons, json, error) -> Void in
            
            if error != nil
            {
                completionHandler?(success: false , error: error)
            } else
            {
                
                var returnObject = MSOrder (info: json)
                
                completionHandler?(success: true , error: nil)
            }
        }

    }
    
    
    

}

//
//  UserModel.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/7/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSUser: MSObject {
    
    let keyUserName = "UserId"
    let keyPassword = "password"
    let keyEmail = "UserId"
    let keyFirstName = "firstName"
    let keyLastName = "lastName"
    let keyFullName = "DisplayFullname"
    let keyNickName = "DisplayName"
    let keyImage = "image"
    let keyTel = "TelephoneNumber"
    let keyFacebookID = "facebookID"
    let keyUserID = "UserGuid"

    
    var userName: String!
    var password: String!
    var email: String!
    var firstName: String!
    var lastName: String!
    var fullName: String!
    var nickName: String!
    var image: MSImage!
    var info: JSON!
    var tel: String!
    var facebookID: String!

    
    var address: MSAddress?
    
    var isEmailVerified: Bool!
    
    static var _currentUser: AnyObject?
    
//    class func getCurrentUser()-> Self? {
//        return nil
//    }
    
        class var currentUser: MSUser?
        {
            
            if let returnUser = _currentUser as? MSUser
            {
                return returnUser
            }
            return nil
        }
    
    
//    class var currentUser: MSUser {
//        struct Static {
//            static var onceToken: dispatch_once_t = 0
//            static var instance: MSUser? = nil
//        }
//        dispatch_once(&Static.onceToken, { () -> Void in
//            Static.instance = MSUser()
//        })
//        return Static.instance!
//    }
    
    func displayName()-> String {
        return self.nickName
    }
    
//    func login() {
//        
//    }
//    
//   
//    
//    func logout() {
//        
//    }
//    
//    func changePassword() {
//        
//    }
    
    
    func signUpNewUser(completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        APIService.manager.userSignUp(self, completionHandler: completionHandler)
    }
    class func logInWithEmail (email:String,password:String,completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
    
      //  APIService.manager.userLogIn(completionHandler)
        
    }
    class func logInWithUserName (username:String,password:String,completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        
        //  APIService.manager.userLogIn(completionHandler)
        var user = MSUser()
        user.userName = username
        user.password = password
        
        CTAPIService.manager.userLogIn(user, completionHandler: completionHandler)

        
    }
    class func logInWithUFacebook (completionHandler: ((user:MSUser?, error:NSError?) -> Void)?)
    {
        
        //  APIService.manager.userLogIn(completionHandler)
    }
    class func changePassword (newPassword:String,oldPassword:String,completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
        APIService.manager.userChangePassword(newPassword, oldPassword: oldPassword, completionHandler: completionHandler)
    }
    class func forgotPassword (completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        
       APIService.manager.userForgotPassword(completionHandler)
    }
    class func logOut (completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
       
        APIService.manager.userLogOut(MSUser.currentUser!, completionHandler: { (success, error) -> Void in
            
            if(success)
            {
                self._currentUser = nil
                MSSession.session.clearSession()
                completionHandler?(success:true,error:nil)
            }
            else
            {
                completionHandler?(success:false,error:error)
            }
        })
       
    }
    
    func update(completionHandler: ((success:Bool, error:NSError?) -> Void)?)
    {
        println("userName = \(self.nickName)")
        CTAPIService.manager.updateUserInfo(self, completionHandler: completionHandler)
//        CTAPIService.manager.updateUserInfo(self, completionHandler: { (success, error) -> Void in
//
//           
//            if(success)
//            {
//                CTUser.currentUser?
//                 completionHandler?(success:true,error:nil)
//            }
//            else
//            {
//                 completionHandler?(success:false,error:error)
//            }
//        })
    }
    
    
    
    override func config(info: JSON)
    {
        super.config(info)
        self._id = info[keyUserID].stringValue
     
        self.userName = info[keyUserName].stringValue
        self.email = info[keyEmail].stringValue
        self.firstName = info[keyFirstName].stringValue
        self.lastName = info[keyLastName].stringValue
        self.nickName = info[keyNickName].stringValue
        self.fullName = info[keyFullName].stringValue
        self.info = info
        self.tel = info[keyTel].stringValue
       // var image = MSImage (info:info[keyImage])
       // self.image = image
        
    }
    
}

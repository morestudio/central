//
//  API.swift
//  MoreGlass
//
//  Created by Tum on 6/25/15.
//  Copyright (c) 2015 Nattapong Saengphrom. All rights reserved.
//

import Foundation


struct APIInfo {
    var method: Method
    var endPoint: String
    
    
    // User
    static var UserSignUp: APIInfo                  { return self(method: .POST, endPoint: "/api/Signup") }
    static var UserLogIn: APIInfo                   { return self(method: .POST, endPoint: "/api/AccountCredential/")}
    static var UserLogOut: APIInfo                  { return self(method: .DELETE, endPoint: "/api/AccountCredential/")}
    static var UserForgotPassword: APIInfo          { return self(method: .POST, endPoint: "")}
    static var UserChangePassword: APIInfo          { return self(method: .POST, endPoint: "/api/ChangePassword/")}
    static var UserInfo: APIInfo                    { return self(method: .GET, endPoint: "")}
    static var UserUpdateInfo: APIInfo              { return self(method: .POST, endPoint: "/api/EditInformationUser/")}
        
    // Shipping
    static var ShippingAddressList: APIInfo         { return self(method: .GET, endPoint: "") }
    static var ShippingAddressDetail: APIInfo       { return self(method: .GET, endPoint: "") }
    static var ShippingAddressCreateNew: APIInfo    { return self(method: .POST, endPoint: "") }
    static var ShippingAddressUpdate: APIInfo       { return self(method: .PUT, endPoint: "") }
    static var ShippingAddressSetDefault: APIInfo   { return self(method: .POST, endPoint: "") }
    static var ShippingAddressDelete: APIInfo       { return self(method: .DELETE, endPoint: "") }
    
    // Product
    static var ProductDetail: APIInfo               { return self(method: .GET, endPoint: "") }
    static var ProductRelatedList: APIInfo          { return self(method: .GET, endPoint: "") }
    static var ProductRelatedMagicSearch: APIInfo          { return self(method: .GET, endPoint: "api/ProductRelatedMagicSearch/") }
    static var ProductBestSellerList: APIInfo       { return self(method: .GET, endPoint: "/api/ProductBestSeller/") }
    static var ProductSimilarList: APIInfo          { return self(method: .GET, endPoint: "") }
    static var PromotionForMobile: APIInfo          { return self(method: .GET, endPoint: "/api/PromotionForMobile") }
    static var ProductPromotionHome: APIInfo          { return self(method: .GET, endPoint: "/api/ProductPromotionHome") }
    // Category
    static var CategoryList: APIInfo                { return self(method: .GET, endPoint: "") }
    static var CategoryDetail: APIInfo              { return self(method: .GET, endPoint: "") }
    static var CategoryProductsList: APIInfo        { return self(method: .GET, endPoint: "") }
    
    // WISH
     static var WishAddProduct: APIInfo              { return self(method: .POST, endPoint: "") }
    
    // Cart
    static var CartProductList: APIInfo             { return self(method: .GET, endPoint: "") }
    static var CartAddProduct: APIInfo              { return self(method: .POST, endPoint: "") }
    static var CartRemoveProduct: APIInfo           { return self(method: .DELETE, endPoint: "") }
    static var CartUpdateProduct: APIInfo           { return self(method: .PUT, endPoint: "") }
    static var CartCheckOut: APIInfo             { return self(method: .PUT, endPoint: "") }
    static var CartClearAllProduct: APIInfo         { return self(method: .DELETE, endPoint: "") }
    // Coupon
    static var CartAddCoupon: APIInfo               { return self(method: .POST, endPoint: "") }
    
    // Store
    static var StoresList: APIInfo                  { return self(method: .GET, endPoint: "") }
    static var StoreDetail: APIInfo                 { return self(method: .GET, endPoint: "") }
    static var ShopsList: APIInfo                   { return self(method: .GET, endPoint: "") }
    static var ShopDetail: APIInfo                  { return self(method: .GET, endPoint: "") }
    
    // Search
    static var SearchProductByName: APIInfo         { return self(method: .GET, endPoint: "") }
    static var SearchProductByBarcode: APIInfo      { return self(method: .GET, endPoint: "") }
    
    static var PromotionList: APIInfo               { return self(method: .GET, endPoint: "") }
    static var PromotionDetail: APIInfo             { return self(method: .GET, endPoint: "") }
    
    static var PaymentMethodsList: APIInfo          { return self(method: .GET, endPoint: "") }
    
    static var OrderHistoryList: APIInfo            { return self(method: .GET, endPoint: "") }
    static var OrderDetail: APIInfo                 { return self(method: .GET, endPoint: "") }
    static var OrderCreateNew: APIInfo              { return self(method: .POST, endPoint: "") }
    static var OrderRepurchase: APIInfo             { return self(method: .POST, endPoint: "") }
    
}
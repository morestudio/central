//
//  MSWishlistItem.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/9/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSWishlistItem: MSObject
{
    let keyProduct = "product"
    let keyDateAdded = "dateAdded"
    let keyQuantity = "quantity"
    
    var product: MSProduct!
    var dateAdded: NSDate = NSDate()
    
    var quantity: Int = 1
}

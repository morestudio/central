//
//  MSCoupon.swift
//  Model
//
//  Created by Nattapong Saengphrom on 7/8/2558 BE.
//  Copyright (c) 2558 Nattapong Saengphrom. All rights reserved.
//

import UIKit

class MSCoupon: MSObject {
    var detail: String!
    var expireDate: NSDate!
}

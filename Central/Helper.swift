//
//  Helper.swift
//  Central
//
//  Created by nickay on 7/17/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class Helper: NSObject
{
   
    class func deley(#time: Double, task: dispatch_block_t) {
        let delay = time * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), task)
    }
    class func sizeOfText(text:String?, font:UIFont?, width:CGFloat, lineBreakMode:NSLineBreakMode, numberOfLines:Int) -> CGSize {
        if text == nil || font == nil {
            return CGSizeZero
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.max))
        label.numberOfLines = numberOfLines
        label.lineBreakMode = lineBreakMode
        label.font = font
        label.text = text
        label.sizeToFit()
        
    
        return label.frame.size
    }
}

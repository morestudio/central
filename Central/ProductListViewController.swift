//
//  ProductListViewController.swift
//  Central
//
//  Created by nickay on 7/21/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ProductHeaderViewDelegate {
    
    var products:[MSProduct] = []
    var numberOfItemsPerLine:Int  = 2
    let minSpacing:CGFloat  = 10
    let cellIdentifier: String = "DisplayCell"
 
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: ProductHeaderViewDelegate
    func didPressSwapLayoutToggle(atIndex: Int)
    {
        if(self.numberOfItemsPerLine == 1)
        {
        
            self.numberOfItemsPerLine = 2
        }
        else
        {
            self.numberOfItemsPerLine = 1
        }
        
        for cell in self.collectionView.visibleCells()
        {
            println("self.numberOfItemsPerLine = \(self.numberOfItemsPerLine)")
            if let displayCell = cell as? DisplayCell
            {
                println("displayCell.productCellView = \(displayCell.productCellView)")
                
                displayCell.productCellView?.viewSize = self.getGridViewSize(nil)
                displayCell.setNumberOfItemsPerLine(self.numberOfItemsPerLine)
            }
        }
        self.view.userInteractionEnabled = false
        self.collectionView.performBatchUpdates(nil, completion: { (finished:Bool) -> Void in
            
            if(finished)
            {
                Helper.deley(time: 0.1, task: { () -> Void in
                    self.view.userInteractionEnabled = true
                })
                
            }
        })
    }
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 2
    }
    //   optional
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        if section == 0
        {
            return CGSizeZero
        }
        else
        {
            return CGSizeMake(self.view.frame.size.width, 50)
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0
        {
            return 1
        }
        else
        {
            return self.products.count
        }
        
    }
    
    func collectionView(collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "HeaderSection",
                    forIndexPath: indexPath)
                    as! ProductHeaderView
                headerView.headerAtIndex = indexPath.section
                headerView.delegate = self
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if(indexPath.section == 0 )
        {
            // Headercell
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Headercell", forIndexPath: indexPath) as! UICollectionViewCell
            return cell
            
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! DisplayCell
         cell.productCellView.viewSize = self.getGridViewSize(nil)
        cell.setNumberOfItemsPerLine(self.numberOfItemsPerLine)
        
        let product:MSProduct = self.products[indexPath.row]
       
        cell.productCellView.config(product)
        
        
        return cell
        
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if (indexPath.section == 0)
        {
            let width:CGFloat = (self.view!.frame.size.width)
            return CGSizeMake(width, 200)
            
        }
        else if self.numberOfItemsPerLine > 1
        {
            let numberOfitemsPerLine : CGFloat = CGFloat(self.numberOfItemsPerLine)
            let width:CGFloat = (self.view!.frame.size.width-(minSpacing * (numberOfitemsPerLine - 1 )) ) / numberOfitemsPerLine
      
            return CGSizeMake(width, width*525/300*1.2)
        }
        else
            
        {
            
            let width:CGFloat = (self.view!.frame.size.width)
            
            
            let labelTitleText = self.products[indexPath.row].name
            
            let labelTitleWidth = self.view.frame.size.width - (10 + 80 + 8 + 8 + 8 + 8)
            
            var boldHelveticaFont = UIFont(name: "Helvetica Neue", size: 15)?.fontDescriptor().fontDescriptorWithSymbolicTraits(UIFontDescriptorSymbolicTraits.TraitBold)
            let labelTitleFont = UIFont(descriptor: boldHelveticaFont!, size: 15)
            
            println("labelTitleWidth = \(labelTitleWidth)")
            let labelTitleSize : CGSize = Helper.sizeOfText(labelTitleText, font: labelTitleFont, width: labelTitleWidth, lineBreakMode: NSLineBreakMode.ByWordWrapping, numberOfLines:2)
            
            
            let labelDetailText = self.products[indexPath.row].detailShort
            let labelDetailWidth = self.view.frame.size.width - 80 - 8 - 8 - 8 - 8
            let labelDetailFont = UIFont (name: "HelveticaNeue-Light", size: 15)
            let labelDetailSize : CGSize = Helper.sizeOfText(labelDetailText, font: labelDetailFont, width: labelDetailWidth, lineBreakMode: NSLineBreakMode.ByWordWrapping, numberOfLines:3)
            
            print("labelTitleSize = \(labelTitleSize.height)")
            println(" \(labelDetailSize.height)")
            
           

            return CGSizeMake(width,10 + labelTitleSize.height + 8 + labelDetailSize.height + 3 + 18 + 3 + 18 + 3 + 30 + 10 )
            
        }

        
    }
    
    func getGridViewSize(product:MSProduct!) -> CGSize
    {
        let numberOfitemsPerLine : CGFloat = CGFloat(self.numberOfItemsPerLine)
        let width:CGFloat = (self.view!.frame.size.width-(minSpacing * (numberOfitemsPerLine - 1 )) ) / numberOfitemsPerLine
        
        return CGSizeMake(width, width*525/300)

    }
    // MARK: CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}



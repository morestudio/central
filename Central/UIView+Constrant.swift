//
//  UIView+Constrant.swift
//  OfficemateSwift
//
//  Created by More Studio on 12/24/2557 BE.
//  Copyright (c) 2557 More Studio. All rights reserved.
//

import UIKit

extension UIView {
    func replaceConstraint(layout: NSLayoutAttribute, view: UIView?, constant: CGFloat)
    {
        (self.constraints() as NSArray).enumerateObjectsUsingBlock { (object, idx, stop) -> Void in
            let constrant = object as! NSLayoutConstraint
            if let second: AnyObject = constrant.secondItem {
                if second as? UIView == view && constrant.firstAttribute == layout {
                    constrant.constant = constant
                }
            } else if constrant.firstAttribute == layout {
                 constrant.constant = constant
            }
        }
    }
    

    
}
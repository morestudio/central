//
//  ProductHeader.swift
//  Central
//
//  Created by nickay on 7/20/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class ProductHeaderView: UICollectionReusableView {
    
    weak var delegate:ProductHeaderViewDelegate? = nil
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var layoutButton: UIButton!
    var headerAtIndex = 0
    @IBOutlet weak var showToggleButton: UIButton!

    
    @IBAction func pressShowToggle(sender: AnyObject)
    {
        
        self.delegate?.didPressShowToggle?(headerAtIndex)
    }
    
    @IBAction func pressSwapLayoutToggle(sender: AnyObject)
    {
        self.delegate?.didPressSwapLayoutToggle?(headerAtIndex)
    }
}

@objc protocol ProductHeaderViewDelegate
{
    optional func didPressShowToggle(atIndex:Int)
    optional func didPressSwapLayoutToggle(atIndex:Int)
    
}

//
//  ListProductView.swift
//  Central
//
//  Created by nickay on 7/17/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class ListProductView: UIView
{
    var didConfigLayout : Bool = false

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var normalPriceLabel: UILabel!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    func config(prodcut:MSProduct)
    {
        prodcut.largeImage?.getImage(self.imageView, completionHandler: { (image, error) -> Void in
            
            if(image != nil)
            {
                self.imageView.image = image
            }
            else
            {
                println("error = \(error)")
            }
        })
        self.nameLabel.text = prodcut.name
        self.detailLabel.text = prodcut.detailShort
        
        self.normalPriceLabel.text = "\(prodcut.price) บาท"
        self.salePriceLabel.text = "\(prodcut.price * prodcut.discountPercent / 100) บาท"
        self.addToCartButton.layer.borderColor = UIColor.redColor().CGColor
        self.addToCartButton.layer.borderWidth = 2
        self.addToCartButton.layer.cornerRadius = 4
        
    }
   
    

}

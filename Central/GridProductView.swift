//
//  GridProductView.swift
//  Central
//
//  Created by nickay on 7/16/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class GridProductView: UIView
{
    let defaultWidth : CGFloat = 240
    var didConfigLayout : Bool = false
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var normalPriceLabel: UILabel!

    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    func config(prodcut:MSProduct)
    {
        prodcut.largeImage?.getImage(self.imageView, completionHandler: { (image, error) -> Void in
            
            if(image != nil)
            {
                self.imageView.image = image
            }
            else
            {
                println("error = \(error)")
            }
        })
        self.nameLabel.text = prodcut.name
        self.detailLabel.text = prodcut.detailShort
        self.normalPriceLabel.text = "\(prodcut.price) THB"
        
    }
    override func layoutSubviews()
    {
        if !didConfigLayout
        {
            Helper.deley(time: 0.01, task: { () -> Void in
                
                let scale = self.frame.size.width / self.defaultWidth
                self.nameLabel.font = UIFont(name: self.nameLabel.font.familyName, size: self.nameLabel.font.pointSize*scale)
                self.detailLabel.font = UIFont(name: self.detailLabel.font.familyName, size: self.detailLabel.font.pointSize*scale)
                self.normalPriceLabel.font = UIFont(name: self.normalPriceLabel.font.familyName, size: self.normalPriceLabel.font.pointSize*scale)
                self.addToCartButton.titleLabel?.font = UIFont(name: self.addToCartButton.titleLabel!.font.familyName, size: self.addToCartButton.titleLabel!.font.pointSize*scale)
            })
            self.didConfigLayout = true
            
            
        }
    }

}

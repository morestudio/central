//
//  ProductCellView.swift
//  Central
//
//  Created by nickay on 7/21/15.
//  Copyright (c) 2015 nickay. All rights reserved.
//

import UIKit

class ProductCellView: UIView {


    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var discountedPriceLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var normalPriceLabel: UILabel!
    
    var viewSize:CGSize = CGSizeZero
    @IBOutlet weak var textHolderView: UIView!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func updateLayout (itemsPerLine:Int)
    {
        
        
        
        if(itemsPerLine == 1) // LIST LAYOUT
        {
            
            self.removeConstraints(self.constraints())
            self.addToCartButton.removeConstraints(self.addToCartButton.constraints())
            self.textHolderView.removeConstraints(self.textHolderView.constraints())
            self.discountedPriceLabel.removeConstraints(self.discountedPriceLabel.constraints())
            self.detailLabel.removeConstraints(self.detailLabel.constraints())
            self.nameLabel.removeConstraints(self.nameLabel.constraints())
            self.imageView.removeConstraints(self.imageView.constraints())
            self.normalPriceLabel.removeConstraints(self.normalPriceLabel.constraints())
            
            self.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.addToCartButton.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.textHolderView.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.discountedPriceLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.detailLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.nameLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.normalPriceLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            self.nameLabel.textAlignment = NSTextAlignment.Left
            self.detailLabel.textAlignment = NSTextAlignment.Left
            
            
            let viewDict = ["addToCartButton": self.addToCartButton,
                "discountedPriceLabel":self.discountedPriceLabel,
                "textHolderView":self.textHolderView,
                "detailLabel": self.detailLabel,
                "nameLabel": self.nameLabel,
                "imageView":self.imageView,
                "normalPriceLabel":self.normalPriceLabel]
            
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[imageView(80)]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[imageView(80)]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[textHolderView]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
          //  self.textHolderView.backgroundColor = UIColor.clearColor()
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[imageView]-8-[textHolderView]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            
            
            
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[nameLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[detailLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[normalPriceLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[discountedPriceLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[addToCartButton(100)]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            
            let superViewWidth : CGFloat = superview!.frame.size.width
            let labelTitleWidth  : CGFloat = 166.5
            println("labelTitleWidth = \(labelTitleWidth)")
            let nameLabelSize : CGSize = Helper.sizeOfText(self.nameLabel.text, font: self.nameLabel.font, width: labelTitleWidth, lineBreakMode: self.nameLabel.lineBreakMode, numberOfLines: self.nameLabel.numberOfLines)
            println("self.nameLabel.frame.size.width = \(self.nameLabel.frame.size.width)")
            let detailLabelSize : CGSize = Helper.sizeOfText(self.detailLabel.text, font: self.detailLabel.font, width: labelTitleWidth, lineBreakMode: self.detailLabel.lineBreakMode, numberOfLines: self.detailLabel.numberOfLines)
            
             println("self.detailLabel = \(self.detailLabel.font)")
            
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[nameLabel(\(nameLabelSize.height))]-8-[detailLabel(\(detailLabelSize.height))]-3-[normalPriceLabel(18)]-3-[discountedPriceLabel(18)]-3-[addToCartButton(30)]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            self.discountedPriceLabel.hidden = false
            
            
            
            
        }
        else if (itemsPerLine > 1)// GRID LAYOUT
        {
            self.removeConstraints(self.constraints())
            self.addToCartButton.removeConstraints(self.addToCartButton.constraints())
            self.textHolderView.removeConstraints(self.textHolderView.constraints())
            self.discountedPriceLabel.removeConstraints(self.discountedPriceLabel.constraints())
            self.detailLabel.removeConstraints(self.detailLabel.constraints())
            self.nameLabel.removeConstraints(self.nameLabel.constraints())
            self.imageView.removeConstraints(self.imageView.constraints())
            self.normalPriceLabel.removeConstraints(self.normalPriceLabel.constraints())
            
            self.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.addToCartButton.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.textHolderView.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.discountedPriceLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.detailLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.nameLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
            self.normalPriceLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
            let viewDict = ["addToCartButton": self.addToCartButton,
                "discountedPriceLabel":self.discountedPriceLabel,
                "textHolderView":self.textHolderView,
                "detailLabel": self.detailLabel,
                "nameLabel": self.nameLabel,
                "imageView":self.imageView,
                "normalPriceLabel":self.normalPriceLabel]
            
            self.nameLabel.textAlignment = NSTextAlignment.Center
            self.detailLabel.textAlignment = NSTextAlignment.Center
            
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[imageView]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[textHolderView]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            let imageViewHeight = 257 * self.viewSize.width / 194
            let textHolderViewHeight =  self.viewSize.height / 2 * 1.1
            
            println("self.viewSize.width = \(self.viewSize.width)")
            //   self.textHolderView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[imageView(\(imageViewHeight))]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textHolderView(\(textHolderViewHeight))]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
   
            
            let labelWidth = self.viewSize.width - 8 - 8
            let nameLabelSize : CGSize = Helper.sizeOfText(self.nameLabel.text, font: self.nameLabel.font, width: labelWidth, lineBreakMode: self.nameLabel.lineBreakMode, numberOfLines: self.nameLabel.numberOfLines)
            let detailLabelSize : CGSize = Helper.sizeOfText(self.detailLabel.text, font: self.detailLabel.font, width: labelWidth, lineBreakMode: self.detailLabel.lineBreakMode, numberOfLines: self.detailLabel.numberOfLines)
            
            self.discountedPriceLabel.hidden = true
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[nameLabel(\(nameLabelSize.height))]-8-[detailLabel(\(detailLabelSize.height))]-8-[normalPriceLabel(18)]-8-[addToCartButton(30)]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[nameLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[detailLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[normalPriceLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-\(self.viewSize.width*0.2)-[addToCartButton]-\(self.viewSize.width*0.2)-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            /*
            
            
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[imageView]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[textHolderView]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            let imageViewHeight = 257 * self.frame.size.width / 194
            let textHolderViewHeight =  self.frame.size.height / 2
            
            println("imageViewHeigh = \(imageViewHeight)")
         //   self.textHolderView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[imageView(\(imageViewHeight))]", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textHolderView(\(textHolderViewHeight))]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[textHolderView(\(textHolderViewHeight))]-0-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            let nameLabelSize : CGSize = Helper.sizeOfText(self.nameLabel.text, font: self.nameLabel.font, width: self.nameLabel.frame.size.width, lineBreakMode: self.nameLabel.lineBreakMode, numberOfLines: 2)
            let detailLabelSize : CGSize = Helper.sizeOfText(self.detailLabel.text, font: self.detailLabel.font, width: self.detailLabel.frame.size.width, lineBreakMode: self.detailLabel.lineBreakMode, numberOfLines: 2)
            
            self.discountedPriceLabel.hidden = true
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[nameLabel(\(nameLabelSize.height))]-8-[detailLabel(\(detailLabelSize.height))]-8-[normalPriceLabel(18)]-8-[addToCartButton(30)]|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[nameLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[detailLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[normalPriceLabel]-8-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            self.textHolderView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-\(self.frame.size.width*0.2)-[addToCartButton]-\(self.frame.size.width*0.2)-|", options: NSLayoutFormatOptions(0), metrics: nil, views: viewDict))
            
            
            */
        }
        
//        self.fadeDown { (animation, success) -> Void in
//            if(success)
//            {
//                self.fadeUp(nil)
//            }
//        }
        
       

    }
    func config(prodcut:MSProduct)
    {
        prodcut.largeImage?.getImage(self.imageView, completionHandler: { (image, error) -> Void in
            
            if(image != nil)
            {
                self.imageView.image = image
            }
            else
            {
                println("error = \(error)")
            }
        })
        self.nameLabel.text = prodcut.name
        self.detailLabel.text = prodcut.detailShort
        
        self.normalPriceLabel.text = "\(prodcut.price) บาท"
        self.discountedPriceLabel.text = "\(prodcut.price * prodcut.discountPercent / 100) บาท"
        self.addToCartButton.layer.borderColor = UIColor.redColor().CGColor
        self.addToCartButton.layer.borderWidth = 2
        self.addToCartButton.layer.cornerRadius = 4
        
        
    }

}
